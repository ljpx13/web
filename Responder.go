package web

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/ljpx13/logging"
)

// Responder is a higher-level abstraction over an *ExtendedResponseWriter that
// is recommended for use in most cases.
type Responder struct {
	ew                  *ExtendedResponseWriter
	httpProblemProducer *HTTPProblemProducer
	logger              logging.Logger
}

// NewResponder creates a new responder from the provided
// ExtendedResponseWriter.
func NewResponder(ew *ExtendedResponseWriter, httpProblemProducer *HTTPProblemProducer, logger logging.Logger) *Responder {
	return &Responder{
		ew:                  ew,
		httpProblemProducer: httpProblemProducer,
		logger:              logger,
	}
}

// Respond takes ownership of the ResponseWriter for the request and responds
// immediately without a response body.  If Respond returns false, ownership of
// the ResponseWriter was not given.
func (rs *Responder) Respond(statusCode int) bool {
	if !rs.ew.Acquire() {
		return false
	}

	rs.ew.WriteHeader(statusCode)
	return true
}

// RespondWithJSON takes ownership of the ResponseWriter for the request and
// responds with the provided JSON model.  If RespondWithJSON returns false,
// ownership of the ResponseWriter was not given.
func (rs *Responder) RespondWithJSON(statusCode int, model interface{}) bool {
	if !rs.ew.Acquire() {
		return false
	}

	raw, err := json.Marshal(model)
	if err != nil {
		raw = rs.httpProblemProducer.RawProblemDetailsForJSONMarshalErrorScenario(err)
		statusCode = http.StatusInternalServerError
	}

	rs.ew.Header().Set("Content-Type", "application/json")
	rs.ew.Header().Set("Content-Length", fmt.Sprintf("%v", len(raw)))
	rs.ew.WriteHeader(statusCode)
	rs.ew.Write(raw)

	return true
}

// RespondWithStream takes ownership of the ResponseWriter for the request and
// responds by reading from the provided reader into the response writer.  It
// will attempt to determine the length of the provided reader and include this
// for the Content-Length header.  If RespondWithStream returns false, ownership
// of the ResponseWriter was not given.
func (rs *Responder) RespondWithStream(statusCode int, contentType string, r io.Reader) bool {
	if !rs.ew.Acquire() {
		return false
	}

	seekable, ok := r.(io.Seeker)
	if ok {
		length, err := seekable.Seek(0, io.SeekEnd)
		if err == nil {
			_, err = seekable.Seek(0, io.SeekStart)
			if !rs.AssertNilError(err) {
				return true
			}

			rs.ew.Header().Set("Content-Length", fmt.Sprintf("%v", length))
		}
	}

	rs.ew.Header().Set("Content-Type", contentType)
	rs.ew.WriteHeader(statusCode)
	io.Copy(rs.ew, r)
	return true
}

// AssertNilError asserts that the provided error is nil, or otherwise responds
// appropriately.  If the call returns true, the error was nil.
func (rs *Responder) AssertNilError(err error) bool {
	if err == nil {
		return true
	}

	rs.logger.Printf("! ERROR %v\n", err)

	problem := rs.httpProblemProducer.InternalServerError(err)
	rs.RespondWithJSON(http.StatusInternalServerError, problem)
	return false
}
