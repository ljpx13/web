package web

import (
	"net/http"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

type InceptionTestFixture struct {
	*TestFixture
}

func SetupInceptionTestFixture() *InceptionTestFixture {
	return &InceptionTestFixture{
		TestFixture: NewTestFixture(newInception, nil),
	}
}

func TestInceptionUnauthorized(t *testing.T) {
	// Arrange.
	fixture := SetupInceptionTestFixture()
	fixture.SetRequest(http.MethodPut, "/inception/no")

	// Act.
	fixture.PerformRequest()

	// Assert.
	test.That(t, fixture.W.Code, is.EqualTo(http.StatusUnauthorized))
}

func TestInceptionAuthorized(t *testing.T) {
	// Arrange.
	fixture := SetupInceptionTestFixture()
	fixture.SetRequest(http.MethodPut, "/inception/yes")

	// Act.
	fixture.PerformRequest()

	// Assert.
	test.That(t, fixture.W.Code, is.EqualTo(http.StatusOK))

	model := &inceptionResponseModel{}
	test.That(t, fixture.GetResponseJSON(model), is.Nil())
	test.That(t, model.Segment, is.EqualTo("yes"))
}

// -----------------------------------------------------------------------------

// Multiple layers of testing happen here...
type inceptionRoute struct {
	req *Requester
	res *Responder
}

var _ Route = &inceptionRoute{}

func newInception(req *Requester, res *Responder) *inceptionRoute {
	return &inceptionRoute{
		req: req,
		res: res,
	}
}

func (*inceptionRoute) Metadata() RouteMetadata {
	return RouteMetadata{
		Method: http.MethodPut,
		Path:   "/inception/*",
	}
}

func (x *inceptionRoute) Handle() {
	segment := x.req.PathParameter(0)
	if segment != "yes" {
		x.res.Respond(http.StatusUnauthorized)
		return
	}

	x.res.RespondWithJSON(http.StatusOK, &inceptionResponseModel{
		Segment: segment,
	})
}

type inceptionResponseModel struct {
	Segment string `json:"segment"`
}
