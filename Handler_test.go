package web

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	"gitlab.com/ljpx13/logging"
	"gitlab.com/ljpx13/m9r"

	"gitlab.com/ljpx13/di"
	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func SetupHandlerTest() *Handler {
	cfg := UnitTestingConfig()

	c := di.NewContainer()
	c.Register(di.Singleton, func() logging.Logger { return logging.NewTestLogger() })

	return NewHandler(context.Background(), c, cfg, logging.NewTestLogger())
}

func TestHandlerUseNonRoute(t *testing.T) {
	// Arrange.
	handler := SetupHandlerTest()

	// Act and Assert.
	func() {
		defer func() {
			r := recover()
			test.That(t, r, is.EqualTo("the provided route factory function does not return a type that implements web.Route"))
		}()

		handler.Use(func() io.Reader { return nil })
	}()
}

func TestHandlerHandle(t *testing.T) {
	// Arrange.
	handler := SetupHandlerTest()
	handler.Use(newTestRoute1)

	svr := httptest.NewServer(handler)
	defer svr.Close()

	// Act.
	req, err := http.NewRequest(http.MethodPut, fmt.Sprintf("%v/test/asd/one", svr.URL), nil)
	test.That(t, err, is.Nil())

	resp, err := http.DefaultClient.Do(req)
	test.That(t, err, is.Nil())
	defer resp.Body.Close()

	// Assert.
	test.That(t, resp.StatusCode, is.EqualTo(http.StatusAccepted))

	raw, err := ioutil.ReadAll(resp.Body)
	test.That(t, err, is.Nil())
	test.That(t, string(raw), is.EqualTo("Value: asd"))
}

func TestHandlerTimeoutExceeded(t *testing.T) {
	// Arrange.
	handler := SetupHandlerTest()
	handler.Use(newTestRoute2)

	svr := httptest.NewServer(handler)
	defer svr.Close()

	// Act.
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%v/test/15/two", svr.URL), nil)
	test.That(t, err, is.Nil())

	resp, err := http.DefaultClient.Do(req)
	test.That(t, err, is.Nil())
	defer resp.Body.Close()

	// Assert.
	test.That(t, resp.StatusCode, is.EqualTo(http.StatusServiceUnavailable))
}

func TestHandlerTimeoutWithin(t *testing.T) {
	// Arrange.
	handler := SetupHandlerTest()
	handler.Use(newTestRoute2)

	svr := httptest.NewServer(handler)
	defer svr.Close()

	// Act.
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%v/test/5/two", svr.URL), nil)
	test.That(t, err, is.Nil())

	resp, err := http.DefaultClient.Do(req)
	test.That(t, err, is.Nil())
	defer resp.Body.Close()

	// Assert.
	test.That(t, resp.StatusCode, is.EqualTo(http.StatusOK))
	test.That(t, resp.Header.Get("X-Some-Header"), is.EqualTo("some-value"))

	raw, err := ioutil.ReadAll(resp.Body)
	test.That(t, err, is.Nil())
	test.That(t, string(raw), is.EqualTo("Hello, World!"))
}

func TestHandlerNotFound(t *testing.T) {
	// Arrange.
	handler := SetupHandlerTest()

	svr := httptest.NewServer(handler)
	defer svr.Close()

	// Act.
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%v/test/three", svr.URL), nil)
	test.That(t, err, is.Nil())

	resp, err := http.DefaultClient.Do(req)
	test.That(t, err, is.Nil())
	defer resp.Body.Close()

	// Assert.
	test.That(t, resp.StatusCode, is.EqualTo(http.StatusNotFound))
	test.That(t, resp.Header.Get("Content-Type"), is.EqualTo("application/json"))
	test.That(t, resp.Header.Get("Content-Length"), is.EqualTo("114"))

	raw, err := ioutil.ReadAll(resp.Body)
	test.That(t, err, is.Nil())
	test.That(t, string(raw), is.EqualTo(`{"type":"https://tes.ts/http/not-found","title":"Not Found","detail":"The path '/test/three' could not be found."}`))
}

func TestHandlerMethodNotAllowed(t *testing.T) {
	// Arrange.
	handler := SetupHandlerTest()
	handler.Use(newTestRoute3)

	svr := httptest.NewServer(handler)
	defer svr.Close()

	// Act.
	req, err := http.NewRequest(http.MethodDelete, fmt.Sprintf("%v/test/three", svr.URL), nil)
	test.That(t, err, is.Nil())

	resp, err := http.DefaultClient.Do(req)
	test.That(t, err, is.Nil())
	defer resp.Body.Close()

	// Assert.
	test.That(t, resp.StatusCode, is.EqualTo(http.StatusMethodNotAllowed))
	test.That(t, resp.Header.Get("Content-Type"), is.EqualTo("application/json"))
	test.That(t, resp.Header.Get("Content-Length"), is.EqualTo("202"))

	raw, err := ioutil.ReadAll(resp.Body)
	test.That(t, err, is.Nil())
	test.That(t, string(raw), is.EqualTo(`{"type":"https://tes.ts/http/method-not-allowed","title":"Method Not Allowed","detail":"The method 'DELETE' is not valid for use on this route.","specifics":{"allowedMethods":["GET"],"method":"DELETE"}}`))
}

func TestHandlerPanic(t *testing.T) {
	// Arrange.
	handler := SetupHandlerTest()
	handler.Use(newTestRoute3)

	svr := httptest.NewServer(handler)
	defer svr.Close()

	// Act.
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%v/test/three", svr.URL), nil)
	test.That(t, err, is.Nil())

	resp, err := http.DefaultClient.Do(req)
	test.That(t, err, is.Nil())
	defer resp.Body.Close()

	// Assert.
	test.That(t, resp.StatusCode, is.EqualTo(http.StatusInternalServerError))
	test.That(t, resp.Header.Get("Content-Type"), is.EqualTo("application/json"))
	test.That(t, resp.Header.Get("Content-Length"), is.EqualTo("177"))

	raw, err := ioutil.ReadAll(resp.Body)
	test.That(t, err, is.Nil())
	test.That(t, string(raw), is.EqualTo(`{"type":"https://tes.ts/http/internal-server-error","title":"Internal Server Error","detail":"An internal server error prevented the operation from completing.","error":"ahhh!"}`))
}

// -----------------------------------------------------------------------------

type testRoute1 struct {
	w http.ResponseWriter
	r *http.Request
}

var _ Route = &testRoute1{}

func newTestRoute1(w http.ResponseWriter, r *http.Request) *testRoute1 {
	return &testRoute1{
		w: w,
		r: r,
	}
}

func (*testRoute1) Metadata() RouteMetadata {
	return RouteMetadata{
		Method: http.MethodPut,
		Path:   "/test/*/one",
	}
}

func (x *testRoute1) Handle() {
	wildSegments := m9r.WildSegments(x.r)

	x.w.WriteHeader(http.StatusAccepted)
	x.w.Write([]byte(fmt.Sprintf("Value: %v", wildSegments[0])))
}

// -----------------------------------------------------------------------------

type testRoute2 struct {
	w *ExtendedResponseWriter
	r *http.Request
}

var _ Route = &testRoute2{}

func newTestRoute2(w *ExtendedResponseWriter, r *http.Request) *testRoute2 {
	return &testRoute2{
		w: w,
		r: r,
	}
}

func (x *testRoute2) Metadata() RouteMetadata {
	return RouteMetadata{
		Method:  http.MethodGet,
		Path:    "/test/*/two",
		Timeout: time.Millisecond * 10,
	}
}

func (x *testRoute2) Handle() {
	wildSegments := m9r.WildSegments(x.r)
	v, err := strconv.ParseUint(wildSegments[0], 10, 64)
	if err != nil {
		panic("bad sleep mills")
	}

	time.Sleep(time.Millisecond * time.Duration(v))
	if !x.w.Acquire() {
		return
	}

	x.w.Header().Set("X-Some-Header", "some-value")
	x.w.WriteHeader(http.StatusOK)
	x.w.Write([]byte("Hello, World!"))
}

// -----------------------------------------------------------------------------

type testRoute3 struct {
	w *ExtendedResponseWriter
	r *http.Request
}

var _ Route = &testRoute3{}

func newTestRoute3(w *ExtendedResponseWriter, r *http.Request) *testRoute3 {
	return &testRoute3{
		w: w,
		r: r,
	}
}

func (x *testRoute3) Metadata() RouteMetadata {
	return RouteMetadata{
		Method: http.MethodGet,
		Path:   "/test/three",
	}
}

func (x *testRoute3) Handle() {
	panic("ahhh!")
}
