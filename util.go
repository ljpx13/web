package web

import (
	"fmt"
	"time"
)

// ByteSizeToFriendlyString returns the provided byte length as a human-friendly
// string e.g. 1024 => 1.00 kB.
func ByteSizeToFriendlyString(length uint64) string {
	floatLength := float64(length)

	prefixes := []string{" B", "kB", "MB", "GB", "TB"}
	prefixIndex := 0

	for floatLength >= 1024 && prefixIndex < len(prefixes)-1 {
		floatLength /= 1024
		prefixIndex++
	}

	return fmt.Sprintf("%.2f %v", floatLength, prefixes[prefixIndex])
}

// DurationToFriendlyString returns the provided duration as a human-friendly
// string e.g. 1500 => 1.50 µs.
func DurationToFriendlyString(dur time.Duration) string {
	if dur < time.Minute {
		floatDur := float64(dur)

		prefixes := []string{"ns", "µs", "ms", " s"}
		index := 0

		for floatDur >= 1000 && index < len(prefixes)-1 {
			floatDur /= 1000
			index++
		}

		return fmt.Sprintf("%.2f%v", floatDur, prefixes[index])
	}

	return fmt.Sprintf("%v", dur)
}

// UnitTestingConfig returns a *web.Config suitable for unit testing.
func UnitTestingConfig() *Config {
	return &Config{
		ProblemDetailsTypePrefix: "https://tes.ts",
		DebuggingEnabled:         true,
		JSONContentLengthLimit:   1 << 20,
		DefaultRouteTimeout:      time.Second * 30,
	}
}
