package web

import (
	"fmt"

	"gitlab.com/ljpx13/problem"
)

// HTTPProblemProducer produces Problem Details structures for HTTP-related
// problems.
type HTTPProblemProducer struct {
	cfg *Config
}

// NewHTTPProblemProducer creates a new HTTPProblemProducer using the provided
// config.
func NewHTTPProblemProducer(cfg *Config) *HTTPProblemProducer {
	return &HTTPProblemProducer{
		cfg: cfg,
	}
}

// Produce produces a skeleton *problem.Details with the provided type-suffix.
func (p *HTTPProblemProducer) Produce(typeSuffix string) *problem.Details {
	return &problem.Details{
		Type: fmt.Sprintf("%v/http/%v", p.cfg.ProblemDetailsTypePrefix, typeSuffix),
	}
}

// InternalServerError produces a Problem that describes an internal server
// error.
func (p *HTTPProblemProducer) InternalServerError(err error) *problem.Details {
	problem := p.Produce("internal-server-error")
	problem.Title = "Internal Server Error"
	problem.Detail = "An internal server error prevented the operation from completing."

	if p.cfg.DebuggingEnabled {
		problem.AttachError(err)
	}

	return problem
}

// MethodNotAllowed produces a Problem that describes the scenario when a call
// is made to a route that does not support the HTTP method used.
func (p *HTTPProblemProducer) MethodNotAllowed(method string, allowedMethods []string) *problem.Details {
	problem := p.Produce("method-not-allowed")
	problem.Title = "Method Not Allowed"
	problem.Detail = fmt.Sprintf("The method '%v' is not valid for use on this route.", method)
	problem.Specifics = map[string]interface{}{
		"method":         method,
		"allowedMethods": allowedMethods,
	}

	return problem
}

// NotFound produces a Problem that describes the scenario when a call is made
// for a resource that cannot be found.
func (p *HTTPProblemProducer) NotFound(typeIdentifier string, literal string) *problem.Details {
	problem := p.Produce("not-found")
	problem.Title = "Not Found"
	problem.Detail = fmt.Sprintf("The %v '%v' could not be found.", typeIdentifier, literal)
	return problem
}

// ServiceUnavailable produces a Problem that describes the scenario when the
// service is unavailable for a generic reason that is not an internal server
// error.
func (p *HTTPProblemProducer) ServiceUnavailable(because string, err error) *problem.Details {
	problem := p.Produce("service-unavailable")
	problem.Title = "Service Unavailable"
	problem.Detail = fmt.Sprintf("The operation could not complete because the service, or a dependency, became unavailable.")

	if because != "" {
		problem.Specifics = because
	}

	if p.cfg.DebuggingEnabled {
		problem.AttachError(err)
	}

	return problem
}

// UnsupportedMediaType produces a Problem that describes the scenario when a
// request is made with a Content-Type that is not supported on the route.
func (p *HTTPProblemProducer) UnsupportedMediaType(contentType string, supportedContentTypes []string) *problem.Details {
	problem := p.Produce("unsupported-media-type")
	problem.Title = "Unsupported Media Type"
	problem.Detail = fmt.Sprintf("The Content-Type '%v' is not supported by this route.", contentType)
	problem.Specifics = map[string]interface{}{
		"contentType":           contentType,
		"supportedContentTypes": supportedContentTypes,
	}
	return problem
}

// RequestEntityTooLarge produces a problem that describes the scenario when a
// request is made with a request body that is too large.
func (p *HTTPProblemProducer) RequestEntityTooLarge(actual, max int64) *problem.Details {
	problem := p.Produce("request-entity-too-large")
	problem.Title = "Request Entity Too Large"
	problem.Detail = "The body of the request exceeds a configured limit."
	problem.Specifics = map[string]interface{}{
		"bodySize": ByteSizeToFriendlyString(uint64(actual)),
	}

	if max != 0 {
		problem.Specifics.(map[string]interface{})["maximumAllowed"] = ByteSizeToFriendlyString(uint64(max))
	}

	return problem
}

// LengthRequired produces a problem that describes the scenario when a
// Content-Length header is expected but not provided.
func (p *HTTPProblemProducer) LengthRequired() *problem.Details {
	problem := p.Produce("length-required")
	problem.Title = "Length Required"
	problem.Detail = "A Content-Length header was absent where one was expected."
	return problem
}

// BadRequest produces a generic problem that describes the scenario when a
// request is "bad" - for whatever reason.
func (p *HTTPProblemProducer) BadRequest(because string, err error) *problem.Details {
	problem := p.Produce("bad-request")
	problem.Title = "Bad Request"
	problem.Detail = "A malformed or otherwise incorrect request was made."

	if because != "" {
		problem.Specifics = because
	}

	if p.cfg.DebuggingEnabled {
		problem.AttachError(err)
	}

	return problem
}

// UnprocessableEntity produces a problem that describes the scenario when an
// entity was provided in a correct format but contained data that was not
// acceptable.
func (p *HTTPProblemProducer) UnprocessableEntity(what, why string) *problem.Details {
	problem := p.Produce("unprocessable-entity")
	problem.Title = "Unprocessable Entity"
	problem.Detail = "The provided entity was valid but contained some data that was not acceptable."
	problem.Specifics = map[string]interface{}{
		"what": what,
		"why":  why,
	}

	return problem
}

// RawProblemDetailsForJSONMarshalErrorScenario is used for special cases where
// a JSON marshal call fails and we need to construct a problem details response
// manually.
func (p *HTTPProblemProducer) RawProblemDetailsForJSONMarshalErrorScenario(err error) []byte {
	jsonMarshalErrorFormatString := `{"type":"%v/http/internal-server-error","title":"Internal Server Error","detail":"An internal server error prevented the operation from completing."%v}`

	errStr := ""
	if p.cfg.DebuggingEnabled && err != nil {
		errStr = fmt.Sprintf(`,"error":"%v"`, err.Error())
	}

	return []byte(fmt.Sprintf(jsonMarshalErrorFormatString, p.cfg.ProblemDetailsTypePrefix, errStr))
}
