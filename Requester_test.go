package web

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/ljpx13/logging"
	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func SetupRequesterTest(w *httptest.ResponseRecorder, r *http.Request) *Requester {
	cfg := UnitTestingConfig()
	httpProblemProducer := NewHTTPProblemProducer(cfg)
	return NewRequester(r, NewResponder(NewExtendedResponseWriter(w), httpProblemProducer, logging.NewTestLogger()), httpProblemProducer, cfg)
}

func TestRequesterAssertContentTypeSuccess(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/test", nil)
	requester := SetupRequesterTest(w, r)

	r.Header.Set("Content-Type", "application/json")

	// Act.
	passed := requester.AssertContentType("application/json")

	// Assert.
	test.That(t, passed, is.True())
	test.That(t, w.Code, is.EqualTo(200))
}

func TestRequesterAssertContentTypeFailure(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/test", nil)
	requester := SetupRequesterTest(w, r)

	r.Header.Set("Content-Type", "image/png")

	// Act.
	passed := requester.AssertContentType("application/json")

	// Assert.
	test.That(t, passed, is.False())
	test.That(t, w.Code, is.EqualTo(http.StatusUnsupportedMediaType))
	test.That(t, w.Header().Get("Content-Length"), is.EqualTo("243"))
	test.That(t, string(w.Body.Bytes()), is.EqualTo(`{"type":"https://tes.ts/http/unsupported-media-type","title":"Unsupported Media Type","detail":"The Content-Type 'image/png' is not supported by this route.","specifics":{"contentType":"image/png","supportedContentTypes":["application/json"]}}`))
}

func TestRequesterAssertContentLengthNoneProvided(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/test", nil)
	requester := SetupRequesterTest(w, r)

	// Act.
	passed := requester.AssertContentLength(12, true)

	// Assert.
	test.That(t, passed, is.False())
	test.That(t, w.Code, is.EqualTo(http.StatusLengthRequired))
	test.That(t, w.Header().Get("Content-Length"), is.EqualTo("142"))
	test.That(t, string(w.Body.Bytes()), is.EqualTo(`{"type":"https://tes.ts/http/length-required","title":"Length Required","detail":"A Content-Length header was absent where one was expected."}`))
}

func TestRequesterAssertContentLengthTooLargeExposingMax(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/test", bytes.NewBufferString("Hello, World!"))
	requester := SetupRequesterTest(w, r)

	// Act.
	passed := requester.AssertContentLength(12, true)

	// Assert.
	test.That(t, passed, is.False())
	test.That(t, w.Code, is.EqualTo(http.StatusRequestEntityTooLarge))
	test.That(t, w.Header().Get("Content-Length"), is.EqualTo("217"))
	test.That(t, string(w.Body.Bytes()), is.EqualTo(`{"type":"https://tes.ts/http/request-entity-too-large","title":"Request Entity Too Large","detail":"The body of the request exceeds a configured limit.","specifics":{"bodySize":"13.00  B","maximumAllowed":"12.00  B"}}`))
}

func TestRequesterAssertContentLengthTooLargeNotExposingMax(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/test", bytes.NewBufferString("Hello, World!"))
	requester := SetupRequesterTest(w, r)

	// Act.
	passed := requester.AssertContentLength(12, false)

	// Assert.
	test.That(t, passed, is.False())
	test.That(t, w.Code, is.EqualTo(http.StatusRequestEntityTooLarge))
	test.That(t, w.Header().Get("Content-Length"), is.EqualTo("189"))
	test.That(t, string(w.Body.Bytes()), is.EqualTo(`{"type":"https://tes.ts/http/request-entity-too-large","title":"Request Entity Too Large","detail":"The body of the request exceeds a configured limit.","specifics":{"bodySize":"13.00  B"}}`))
}

func TestRequesterAssertContentLengthSuccess(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/test", bytes.NewBufferString("Hello, World!"))
	requester := SetupRequesterTest(w, r)

	// Act.
	passed := requester.AssertContentLength(13, false)

	// Assert.
	test.That(t, passed, is.True())
	test.That(t, w.Code, is.EqualTo(200))
}

func TestRequesterFromJSONInvalidJSON(t *testing.T) {
	// Arrange.
	modelJSON := `{"name":"John","age":18`

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/test", bytes.NewBufferString(modelJSON))
	requester := SetupRequesterTest(w, r)

	r.Header.Set("Content-Type", "application/json")

	// Act.
	model := &testModel{}
	passed := requester.FromJSON(model)

	// Assert.
	test.That(t, passed, is.False())
	test.That(t, w.Code, is.EqualTo(http.StatusBadRequest))
	test.That(t, w.Header().Get("Content-Length"), is.EqualTo("198"))
	test.That(t, string(w.Body.Bytes()), is.EqualTo(`{"type":"https://tes.ts/http/bad-request","title":"Bad Request","detail":"A malformed or otherwise incorrect request was made.","specifics":"failed to decode provided JSON","error":"unexpected EOF"}`))
}

func TestRequesterFromJSONInvalidModel(t *testing.T) {
	// Arrange.
	modelJSON := `{"name":"John","age":17}`

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/test", bytes.NewBufferString(modelJSON))
	requester := SetupRequesterTest(w, r)

	r.Header.Set("Content-Type", "application/json")

	// Act.
	model := &testModel{}
	passed := requester.FromJSON(model)

	// Assert.
	test.That(t, passed, is.False())
	test.That(t, w.Code, is.EqualTo(http.StatusUnprocessableEntity))
	test.That(t, w.Header().Get("Content-Length"), is.EqualTo("244"))
	test.That(t, string(w.Body.Bytes()), is.EqualTo(`{"type":"https://tes.ts/http/unprocessable-entity","title":"Unprocessable Entity","detail":"The provided entity was valid but contained some data that was not acceptable.","specifics":{"what":"age","why":"age must be 18 or higher, but was 17"}}`))
}

func TestRequesterFromJSONSuccess(t *testing.T) {
	// Arrange.
	modelJSON := `{"name":"John","age":18}`

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/test", bytes.NewBufferString(modelJSON))
	requester := SetupRequesterTest(w, r)

	r.Header.Set("Content-Type", "application/json")

	// Act.
	model := &testModel{}
	passed := requester.FromJSON(model)

	// Assert.
	test.That(t, passed, is.True())
	test.That(t, model.Name, is.EqualTo("John"))
	test.That(t, model.Age, is.EqualTo(18))
}

// -----------------------------------------------------------------------------

type testModel struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

var _ Purifiable = &testModel{}

func (model *testModel) Purify() (string, error) {
	if model.Age < 18 {
		return "age", fmt.Errorf("age must be 18 or higher, but was %v", model.Age)
	}

	return "", nil
}
