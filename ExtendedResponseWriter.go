package web

import (
	"net/http"
	"sync/atomic"
)

// ExtendedResponseWriter wraps an existing http.ResponseWriter with some
// extended functionality.
type ExtendedResponseWriter struct {
	w http.ResponseWriter

	l          uint32
	statusCode int
	volume     uint64
}

var _ http.ResponseWriter = &ExtendedResponseWriter{}

// NewExtendedResponseWriter returns a new ExtendedResponseWriter wrapping the
// provided http.ResponseWriter.
func NewExtendedResponseWriter(w http.ResponseWriter) *ExtendedResponseWriter {
	return &ExtendedResponseWriter{
		w: w,
	}
}

// Acquire returns true if the current call is the first on this instance.  It
// is used to determine if another goroutine is about to write a response.
func (w *ExtendedResponseWriter) Acquire() bool {
	return atomic.AddUint32(&w.l, 1) == 1
}

// Header returns the underlying set of headers.
func (w *ExtendedResponseWriter) Header() http.Header {
	return w.w.Header()
}

// WriteHeader writes the header of the request.
func (w *ExtendedResponseWriter) WriteHeader(statusCode int) {
	if w.statusCode != 0 {
		return
	}

	w.w.WriteHeader(statusCode)
	w.statusCode = statusCode
}

// Write writes to the underlying response writer.
func (w *ExtendedResponseWriter) Write(b []byte) (int, error) {
	n, err := w.w.Write(b)
	w.volume += uint64(n)
	return n, err
}

// StatusCode returns the status code that was written to this
// ExtendedResponseWriter.  If the returned bool is false, a status code has
// not yet been written.
func (w *ExtendedResponseWriter) StatusCode() (int, bool) {
	if w.statusCode == 0 {
		return 0, false
	}

	return w.statusCode, true
}

// Volume returns the volume of data that has been written through this
// ExtendedResponseWriter, not including headers.
func (w *ExtendedResponseWriter) Volume() uint64 {
	return w.volume
}
