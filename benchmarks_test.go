package web

import (
	"bytes"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func BenchmarkHandler(b *testing.B) {
	handler := SetupHandlerTest()
	handler.Use(newBenchmarkRoute)

	b.ResetTimer()

	for n := 0; n < b.N; n++ {
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodPost, "/benchmark/asd", bytes.NewBufferString(`{"name":"John Smith"}`))
		r.Header.Set("Content-Type", "application/json")

		handler.ServeHTTP(w, r)
		test.That(b, w.Code, is.EqualTo(http.StatusOK))
		test.That(b, string(w.Body.Bytes()), is.EqualTo(`{"lowercaseName":"john smith","segment":"asd"}`))
	}
}

// -----------------------------------------------------------------------------

type benchmarkRoute struct {
	req *Requester
	res *Responder
}

var _ Route = &benchmarkRoute{}

func newBenchmarkRoute(req *Requester, res *Responder) *benchmarkRoute {
	return &benchmarkRoute{
		req: req,
		res: res,
	}
}

func (*benchmarkRoute) Metadata() RouteMetadata {
	return RouteMetadata{
		Method: http.MethodPost,
		Path:   "/benchmark/*",
	}
}

func (x *benchmarkRoute) Handle() {
	model := &benchmarkRequestModel{}
	if !x.req.FromJSON(model) {
		return
	}

	resModel := &benchmarkResponseModel{
		LowercaseName: strings.ToLower(model.Name),
		Segment:       x.req.PathParameter(0),
	}

	x.res.RespondWithJSON(http.StatusOK, resModel)
}

type benchmarkRequestModel struct {
	Name string `json:"name"`
}

var _ Purifiable = &benchmarkRequestModel{}

func (m *benchmarkRequestModel) Purify() (string, error) {
	if strings.TrimSpace(m.Name) == "" {
		return "name", errors.New("was not provided")
	}

	return "", nil
}

type benchmarkResponseModel struct {
	LowercaseName string `json:"lowercaseName"`
	Segment       string `json:"segment"`
}
