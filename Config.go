package web

import "time"

// Config is passed to NewHandler to dictate how a Handler behaves.
type Config struct {
	ProblemDetailsTypePrefix string
	DebuggingEnabled         bool
	JSONContentLengthLimit   int64
	DefaultRouteTimeout      time.Duration
}
