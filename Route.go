package web

// Route defines the methods that any route must implement.
type Route interface {
	Metadata() RouteMetadata
	Handle()
}
