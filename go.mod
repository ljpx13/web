module gitlab.com/ljpx13/web

go 1.14

require (
	gitlab.com/ljpx13/di v0.4.5
	gitlab.com/ljpx13/logging v0.1.2
	gitlab.com/ljpx13/m9r v0.1.2
	gitlab.com/ljpx13/problem v0.1.1
	gitlab.com/ljpx13/test v0.1.6
)
