package web

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/ljpx13/logging"
	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func SetupResponderTest(w *httptest.ResponseRecorder) *Responder {
	cfg := UnitTestingConfig()
	return NewResponder(NewExtendedResponseWriter(w), NewHTTPProblemProducer(cfg), logging.NewTestLogger())
}

func TestResponderReturnsFalseWhenOwnershipOfResponseWriterAlreadyTaken(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	responder := SetupResponderTest(w)

	// Pre-condition.
	test.That(t, responder.ew.Acquire(), is.True())

	// Act.
	v1 := responder.Respond(http.StatusOK)
	v2 := responder.RespondWithJSON(http.StatusOK, struct{}{})
	v3 := responder.RespondWithStream(http.StatusOK, "application/octet-stream", bytes.NewBuffer([]byte{}))

	// Assert.
	test.That(t, v1, is.False())
	test.That(t, v2, is.False())
	test.That(t, v3, is.False())
}

func TestResponderRespond(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	responder := SetupResponderTest(w)

	// Act.
	v := responder.Respond(http.StatusNoContent)

	// Assert.
	test.That(t, v, is.True())
	test.That(t, w.Code, is.EqualTo(http.StatusNoContent))
}

func TestResponderRespondWithJSONSuccess(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	responder := SetupResponderTest(w)

	// Act.
	v := responder.RespondWithJSON(http.StatusMethodNotAllowed, struct {
		Reason string
	}{
		Reason: "Method Not Allowed",
	})

	// Assert.
	test.That(t, v, is.True())
	test.That(t, w.Code, is.EqualTo(http.StatusMethodNotAllowed))
	test.That(t, w.Header().Get("Content-Type"), is.EqualTo("application/json"))
	test.That(t, w.Header().Get("Content-Length"), is.EqualTo("31"))
	test.That(t, string(w.Body.Bytes()), is.EqualTo(`{"Reason":"Method Not Allowed"}`))
}

func TestResponderRespondWithJSONFailure(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	responder := SetupResponderTest(w)
	model := &unmarshalable{}

	// Act.
	v := responder.RespondWithJSON(http.StatusOK, model)

	// Assert.
	test.That(t, v, is.True())
	test.That(t, w.Code, is.EqualTo(http.StatusInternalServerError))
	test.That(t, w.Header().Get("Content-Type"), is.EqualTo("application/json"))
	test.That(t, w.Header().Get("Content-Length"), is.EqualTo("242"))
	test.That(t, string(w.Body.Bytes()), is.EqualTo(`{"type":"https://tes.ts/http/internal-server-error","title":"Internal Server Error","detail":"An internal server error prevented the operation from completing.","error":"json: error calling MarshalJSON for type *web.unmarshalable: no can do"}`))
}

func TestResponderRespondWithStream(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	responder := SetupResponderTest(w)
	buf := bytes.NewReader([]byte("Hello, World!"))

	// Act.
	v := responder.RespondWithStream(http.StatusAccepted, "text/plain", buf)

	// Assert.
	test.That(t, v, is.True())
	test.That(t, w.Code, is.EqualTo(http.StatusAccepted))
	test.That(t, w.Header().Get("Content-Type"), is.EqualTo("text/plain"))
	test.That(t, w.Header().Get("Content-Length"), is.EqualTo("13"))
	test.That(t, string(w.Body.Bytes()), is.EqualTo("Hello, World!"))
}

func TestResponderAssertNilErrorWhenNil(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	responder := SetupResponderTest(w)

	// Act.
	v := responder.AssertNilError(nil)

	// Assert.
	test.That(t, v, is.True())
	test.That(t, responder.ew.Acquire(), is.True())
}

func TestResponderAssertNilErrorWhenNotNil(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	responder := SetupResponderTest(w)

	// Act.
	v := responder.AssertNilError(errors.New("oh no"))

	// Assert.
	test.That(t, v, is.False())
	test.That(t, responder.ew.Acquire(), is.False())
	test.That(t, w.Code, is.EqualTo(http.StatusInternalServerError))
	test.That(t, w.Header().Get("Content-Length"), is.EqualTo("177"))
	test.That(t, string(w.Body.Bytes()), is.EqualTo(`{"type":"https://tes.ts/http/internal-server-error","title":"Internal Server Error","detail":"An internal server error prevented the operation from completing.","error":"oh no"}`))
}

// -----------------------------------------------------------------------------

type unmarshalable struct{}

var _ json.Marshaler = &unmarshalable{}

func (u *unmarshalable) MarshalJSON() ([]byte, error) {
	return nil, errors.New("no can do")
}
