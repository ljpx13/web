package web

import (
	"testing"
	"time"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestByteSizeToFriendlyString(t *testing.T) {
	testCases := []struct {
		given    uint64
		expected string
	}{
		{given: 1, expected: "1.00  B"},
		{given: 1023, expected: "1023.00  B"},
		{given: 1024, expected: "1.00 kB"},
		{given: 1536, expected: "1.50 kB"},
		{given: 1048576, expected: "1.00 MB"},
		{given: 3214822145, expected: "2.99 GB"},
	}

	for _, testCase := range testCases {
		actual := ByteSizeToFriendlyString(testCase.given)
		test.That(t, actual, is.EqualTo(testCase.expected))
	}
}

func TestDurationToFriendlyStringDurationCompatible(t *testing.T) {
	testCases := []struct {
		given    time.Duration
		expected string
	}{
		{given: time.Nanosecond, expected: "1.00ns"},
		{given: time.Nanosecond * 999, expected: "999.00ns"},
		{given: time.Nanosecond * 1000, expected: "1.00µs"},
		{given: time.Nanosecond * 1500, expected: "1.50µs"},
		{given: time.Nanosecond * 1999, expected: "2.00µs"},
		{given: time.Nanosecond * 1000000, expected: "1.00ms"},
		{given: time.Second, expected: "1.00 s"},
		{given: time.Second * 90, expected: "1m30s"},
		{given: time.Second * 3601, expected: "1h0m1s"},
	}

	for _, testCase := range testCases {
		actual := DurationToFriendlyString(testCase.given)
		test.That(t, actual, is.EqualTo(testCase.expected))
	}
}
