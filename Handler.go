package web

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"reflect"
	"time"

	"gitlab.com/ljpx13/di"
	"gitlab.com/ljpx13/logging"
	"gitlab.com/ljpx13/m9r"
)

// Handler implements http.Handler and orchestrates a set of Routes.  It is
// thread-safe.
type Handler struct {
	ctx    context.Context
	c      di.Container
	cfg    *Config
	logger logging.Logger

	mplx                *m9r.Multiplexer
	httpProblemProducer *HTTPProblemProducer
}

var _ http.Handler = &Handler{}

var typeRoute = reflect.TypeOf((*Route)(nil)).Elem()

// NewHandler creates a new, empty handler.
func NewHandler(ctx context.Context, roc di.ReadOnlyContainer, cfg *Config, logger logging.Logger) *Handler {
	c := roc.Fork()
	c.Register(di.InstancePerContainer, NewResponder)
	c.Register(di.InstancePerContainer, NewRequester)

	httpProblemProducer := NewHTTPProblemProducer(cfg)
	c.Register(di.Singleton, func() *Config { return cfg })
	c.Register(di.Singleton, NewHTTPProblemProducer)

	return &Handler{
		ctx:    ctx,
		c:      c,
		cfg:    cfg,
		logger: logger,

		mplx:                m9r.NewMultiplexer(),
		httpProblemProducer: httpProblemProducer,
	}
}

// GetSensibleServer returns a sensibly configured *http.Server.
func (h *Handler) GetSensibleServer(addr string) *http.Server {
	return &http.Server{
		Addr:    addr,
		Handler: h,
		BaseContext: func(net.Listener) context.Context {
			return h.ctx
		},
	}
}

// ServeHTTP simply delegates to the underlying multiplexer, handling any
// unexpected scenarios.
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	started := time.Now()
	ew := NewExtendedResponseWriter(w)
	result := h.mplx.ExtendedServeHTTP(ew, r)

	if result != nil {
		responder := NewResponder(ew, h.httpProblemProducer, h.logger)

		switch v := result.(type) {
		case *m9r.NotFoundScenarioResult:
			problem := h.httpProblemProducer.NotFound("path", v.RequestedPath)
			responder.RespondWithJSON(http.StatusNotFound, problem)
		case *m9r.MethodNotAllowedScenarioResult:
			problem := h.httpProblemProducer.MethodNotAllowed(v.RequestedMethod, v.AllowedMethods)
			responder.RespondWithJSON(http.StatusMethodNotAllowed, problem)
		case *m9r.PanicScenarioResult:
			problem := h.httpProblemProducer.InternalServerError(fmt.Errorf("%v", v.PanicResult))
			responder.RespondWithJSON(http.StatusInternalServerError, problem)
		}
	}

	dur := time.Now().Sub(started)
	h.logRequest(ew, dur, r.URL.Path)
}

// Use registers the provided route for use.
func (h *Handler) Use(routeFunc interface{}) {
	ff := di.NewFactoryFunc(routeFunc)
	t := ff.Produces()

	if !t.Implements(typeRoute) {
		panic("the provided route factory function does not return a type that implements web.Route")
	}

	route := reflect.New(t).Elem().Interface().(Route)
	routeMetadata := route.Metadata()
	timeout := routeMetadata.Timeout
	if timeout == 0 {
		timeout = h.cfg.DefaultRouteTimeout
	}

	h.mplx.Use(routeMetadata.Method, routeMetadata.Path, h.buildHTTPHandlerForRouteType(routeFunc, timeout))
}

func (h *Handler) logRequest(ew *ExtendedResponseWriter, dur time.Duration, path string) {
	statusCodeStr := "???"
	statusCode, ok := ew.StatusCode()
	if ok {
		statusCodeStr = fmt.Sprintf("%v", statusCode)
	}

	durStr := DurationToFriendlyString(dur)
	volume := ByteSizeToFriendlyString(ew.Volume())

	str := fmt.Sprintf("→ %v %8v %10v %v\n", statusCodeStr, durStr, volume, path)
	h.logger.Printf(str)
}

func (h *Handler) buildHTTPHandlerForRouteType(routeFunc interface{}, timeout time.Duration) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ew := w.(*ExtendedResponseWriter)
		c := h.c.Fork()

		routeType := c.Register(di.AnchoredSingleton, routeFunc)
		c.Register(di.Singleton, func() *ExtendedResponseWriter { return ew })
		c.Register(di.Singleton, func() http.ResponseWriter { return ew })
		c.Register(di.Singleton, func() *http.Request { return r })

		ctx, cancel := context.WithTimeout(r.Context(), timeout)
		defer cancel()

		r = r.WithContext(ctx)
		c.Register(di.AnchoredSingleton, func() context.Context { return ctx })

		ri, err := c.ResolveType(routeType)
		if err != nil {
			panic(err)
		}

		route := ri.(Route)
		done := make(chan interface{})
		go func() {
			defer func() {
				done <- recover()
			}()

			route.Handle()
		}()

		select {
		case p := <-done:
			if p != nil {
				panic(p)
			}
		case <-ctx.Done():
			responder := NewResponder(ew, h.httpProblemProducer, h.logger)
			problem := h.httpProblemProducer.ServiceUnavailable("a configured timeout was reached", nil)
			responder.RespondWithJSON(http.StatusServiceUnavailable, problem)
		}
	})
}
