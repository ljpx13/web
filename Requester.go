package web

import (
	"encoding/json"
	"net/http"
	"strings"

	"gitlab.com/ljpx13/m9r"
)

// Requester is used to read information in from a request.
type Requester struct {
	r                   *http.Request
	responder           *Responder
	httpProblemProducer *HTTPProblemProducer
	cfg                 *Config

	pathParameters []string
}

// NewRequester creates a new Requester from the provided HTTP request and
// responder.
func NewRequester(r *http.Request, responder *Responder, httpProblemProducer *HTTPProblemProducer, cfg *Config) *Requester {
	return &Requester{
		r:                   r,
		responder:           responder,
		httpProblemProducer: httpProblemProducer,
		cfg:                 cfg,

		pathParameters: m9r.WildSegments(r),
	}
}

// AssertContentType ensures that the content type of the request matches one of
// the content types provided.
func (r *Requester) AssertContentType(allowedContentTypes ...string) bool {
	contentType := r.r.Header.Get("Content-Type")
	contentTypeUppercase := strings.TrimSpace(strings.ToUpper(contentType))

	for _, allowedContentType := range allowedContentTypes {
		if contentTypeUppercase == strings.ToUpper(allowedContentType) {
			return true
		}
	}

	problem := r.httpProblemProducer.UnsupportedMediaType(contentType, allowedContentTypes)
	r.responder.RespondWithJSON(http.StatusUnsupportedMediaType, problem)

	return false
}

// AssertContentLength ensures that a content length was provided, and that it
// is in (0, max].
func (r *Requester) AssertContentLength(max int64, exposeMax bool) bool {
	contentLength := r.r.ContentLength

	if contentLength > max {
		if !exposeMax {
			max = 0
		}

		problem := r.httpProblemProducer.RequestEntityTooLarge(contentLength, max)
		r.responder.RespondWithJSON(http.StatusRequestEntityTooLarge, problem)
		return false
	}

	if contentLength <= 0 {
		problem := r.httpProblemProducer.LengthRequired()
		r.responder.RespondWithJSON(http.StatusLengthRequired, problem)
		return false
	}

	return true
}

// FromJSON retrieves JSON from the request body to place into the provided
// Purifiable.
func (r *Requester) FromJSON(model Purifiable) bool {
	if !r.AssertContentType("application/json") {
		return false
	}

	if !r.AssertContentLength(r.cfg.JSONContentLengthLimit, false) {
		return false
	}

	decoder := json.NewDecoder(r.r.Body)
	err := decoder.Decode(model)
	if err != nil {
		problem := r.httpProblemProducer.BadRequest("failed to decode provided JSON", err)
		r.responder.RespondWithJSON(http.StatusBadRequest, problem)
		return false
	}

	field, err := model.Purify()
	if err != nil {
		problem := r.httpProblemProducer.UnprocessableEntity(field, err.Error())
		r.responder.RespondWithJSON(http.StatusUnprocessableEntity, problem)
		return false
	}

	return true
}

// PathParameter returns the wild path segment at the provided index.
func (r *Requester) PathParameter(index int) string {
	if index >= len(r.pathParameters) {
		return ""
	}

	return r.pathParameters[index]
}

// QueryParameter returns the query parameter with the specified name.
func (r *Requester) QueryParameter(name string) string {
	return r.r.URL.Query().Get(name)
}
