package web

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"

	"gitlab.com/ljpx13/di"
	"gitlab.com/ljpx13/logging"
)

// TestFixture is a type that can be embedded into Route test fixtures to help
// with unit testing.
type TestFixture struct {
	W      *httptest.ResponseRecorder
	R      *http.Request
	C      di.Container
	Logger *logging.TestLogger

	handler *Handler
}

// NewTestFixture creates a TestFixture for the provided routeFunc.
func NewTestFixture(routeFunc interface{}, registrations func(c di.Container)) *TestFixture {
	c := di.NewContainer()
	if registrations != nil {
		registrations(c)
	}

	ctx := context.Background()
	cfg := UnitTestingConfig()
	logger := logging.NewTestLogger()

	c.Register(di.Singleton, func() logging.Logger { return logger })

	handler := NewHandler(ctx, c, cfg, logger)
	handler.Use(routeFunc)

	return &TestFixture{
		W:      httptest.NewRecorder(),
		C:      c,
		Logger: logger,

		handler: handler,
	}
}

// SetRequest sets the request method and path with no body.
func (f *TestFixture) SetRequest(method string, path string) {
	f.R = httptest.NewRequest(method, path, nil)
}

// SetRequestWithJSONModel sets the request method and path and serializes the
// provided Purifiable as the request body.
func (f *TestFixture) SetRequestWithJSONModel(method string, path string, model Purifiable) {
	raw, err := json.Marshal(model)
	if err != nil {
		panic(err)
	}

	f.R = httptest.NewRequest(method, path, bytes.NewBuffer(raw))
	f.R.Header.Set("Content-Type", "application/json")
	f.R.Header.Set("Content-Length", fmt.Sprintf("%v", len(raw)))
}

// SetRequestWithRawBody sets the request method and path and uses the provided
// raw bytes as the request body.
func (f *TestFixture) SetRequestWithRawBody(method string, path string, raw []byte) {
	f.R = httptest.NewRequest(method, path, bytes.NewBuffer(raw))
}

// PerformRequest performs the request.  This should be the last action taken
// before assertions start.
func (f *TestFixture) PerformRequest() {
	f.handler.ServeHTTP(f.W, f.R)
}

// GetResponseJSON reads the response that was written into the provided model.
func (f *TestFixture) GetResponseJSON(model interface{}) error {
	decoder := json.NewDecoder(f.W.Body)
	return decoder.Decode(model)
}
