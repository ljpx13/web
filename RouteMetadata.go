package web

import "time"

// RouteMetadata describes various properties of a route that dictate how it is
// treated by Handler.
type RouteMetadata struct {
	Method  string
	Path    string
	Timeout time.Duration
}
