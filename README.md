![](./icon.png)

# web

Package `web` implements a small web framework focusing on scalability and easy
unit testing.
